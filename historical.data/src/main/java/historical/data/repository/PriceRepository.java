package historical.data.repository;

import historical.data.model.PriceStatistics;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository<PriceStatistics, Long> {
}
