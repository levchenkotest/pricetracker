package historical.data.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import historical.data.model.PriceStatistics;
import historical.data.repository.PriceRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/historical-prices")
public class HistoricalPriceController {

    private final PriceRepository priceRepository;
    private final ObjectMapper objectMapper;
    private final RestTemplate restClientImpl;

    @PostMapping
    public ResponseEntity<String> storePrice(@RequestBody String historicalPrice) throws JsonProcessingException {
        List<PriceStatistics> tickerStatisticsList = objectMapper.readValue(historicalPrice, objectMapper.getTypeFactory()
                .constructCollectionType(List.class, PriceStatistics.class));
        priceRepository.saveAll(tickerStatisticsList);

        String alertDataServiceUrl = "http://localhost:8082/price-alerts";
        ResponseEntity<String> response = restClientImpl.postForEntity(alertDataServiceUrl, tickerStatisticsList, String.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return ResponseEntity.ok("Price alerts send successfully.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to store historical price.");
        }
    }

    @GetMapping
    public List<PriceStatistics> getPrices() {
        return priceRepository.findAll();
    }
}
