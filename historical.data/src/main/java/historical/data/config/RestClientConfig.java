package historical.data.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@AllArgsConstructor
public class RestClientConfig {

    @Bean
    public RestTemplate restClientImpl() {
        //new SpotClientImpl(PrivateConfig.API_KEY, PrivateConfig.SECRET_KEY);
        return new RestTemplate();
    }
}
