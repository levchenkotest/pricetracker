package price.tracking.service;

import com.binance.connector.client.impl.SpotClientImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import price.tracking.model.BinanceSymbol;

import java.util.*;

@Service
@AllArgsConstructor
@Slf4j
@EnableScheduling
public class TickerService {

    private final SpotClientImpl binanceSpotClientImpl;
    private final RestTemplate restClientImpl;

    @Scheduled(fixedRate = 60000)
    public ResponseEntity<String> fetchCryptoPrices() {
        Map<String, Object> tickers = new HashMap<>();
        tickers.put("symbols", new ArrayList<>(Arrays.asList(BinanceSymbol.BTCUSDT.getSymbol(),
                BinanceSymbol.BNBUSDT.getSymbol())));
        String result = binanceSpotClientImpl.createMarket().ticker24H(tickers);

        String historicalDataServiceUrl = "http://localhost:8080/historical-prices";
        ResponseEntity<String> response = restClientImpl.postForEntity(historicalDataServiceUrl, result, String.class);

        if (response.getStatusCode().is2xxSuccessful()) {
            return ResponseEntity.ok("Historical price stored successfully.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to store historical price.");
        }
    }
}
