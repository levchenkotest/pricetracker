# Cryptocurrency Price Tracker

The Cryptocurrency Price Tracker is a Java-based microservices project that tracks the latest cryptocurrency prices using the Binance API. It consists of multiple services for price tracking, storing historical data, and handling price alert notifications. The project is built with Spring Boot and utilizes various Spring Cloud components for service discovery and configuration management.

## Services

- Price Tracking Service: Fetches the latest cryptocurrency prices in real-time from the Binance API. It provides a REST API endpoint to retrieve the latest prices for a given cryptocurrency symbol
- Historical Data Service: Stores and retrieves historical price data. It exposes REST API endpoints to store and retrieve historical prices based on various criteria, such as cryptocurrency symbol and time range.

## Technology Stack

The project utilizes the following technologies and frameworks:
  
 - Java with Spring Boot for building microservices
 - Spring Cloud for implementing features like service discovery and configuration management
 - Spring Security for handling authentication and authorization
 - Eureka for service discovery
 - RabbitMQ for asynchronous communication between microservices
 - MySQL for the database to store historical price data
 - Docker for containerization
