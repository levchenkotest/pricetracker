package price.alerts.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import price.alerts.model.PriceStatistics;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/price-alerts")
public class PriceAlertController {

    @PostMapping
    public ResponseEntity<String> storePrice(@RequestBody List<PriceStatistics> priceStatistics) {
        System.out.println(priceStatistics);
        return ResponseEntity.ok("Price alerts send successfully.");
    }
}
