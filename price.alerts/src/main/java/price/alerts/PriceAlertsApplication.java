package price.alerts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PriceAlertsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PriceAlertsApplication.class, args);
	}

}
